require(devtools)
Sys.setenv("PKG_CXXFLAGS"="-std=c++11")

require(rbed)
toydir = system.file("extdata", package = "rbed")
setwd(toydir)
list.files()
# access fields
rbedobj = Rbed("test")
rbedobj$bedpath
rbedobj$fampath
rbedobj$bimpath
rbedobj$bedstem
rbedobj$nsnp
rbedobj$nindiv
rbedobj$nindivApparent
rbedobj$bytes_snp
rbedobj$snp
# read the whole bed file
x = rbedobj$bedmat.full()
print(x)
# read from snp 1 to snp 3
x = rbedobj$bedmat.snpinter(1, 3)
print(x)
# read from snp 1 to snp 3, choosing only individual 2 and 6
x = rbedobj$bedmat.snpinter.ind(1, 3, c(2, 6))
print(x)

x = rbedobj$bedmat.full()
print(x)
# read snp 1, 3, and 12
x = rbedobj$bedmat.snpvec(c(1, 3, 12))
print(x)
# read snp 1, 3, and 12, choosing only individual 1, 2, and 6
x = rbedobj$bedmat.snpvec.ind(c(1, 3, 12), c(1, 2, 6))
print(x)


x = rbedobj$bedmat.full()
print(x)
# read snp 2, 3, and 5 by name
x = rbedobj$bedmat.snpNameVec(c("snp2", "snp3", "snp5"))
print(x)
# read snp 2, 4, and 6 by SNP names in a file
x = rbedobj$bedmat.snpNameFile("snplist")
print(x)

